#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, time, random
import cfg

WHITE	= '\033[93m■\033[97m'
BLACK	= '\033[96m■\033[97m'
ANT		= '\033[91m■\033[97m'

class Cell:

	def __init__(self, locate, symbol):
		self.symbol = symbol
		self.locate = locate

	def getLocation(self):

		return self.locate

	def getSymbol(self):

		return self.symbol

	def getColor(self):

		if self.symbol == BLACK:	#这里的颜色有问题....
			return cfg.COLOR_WHITE
		else:
			return cfg.COLOR_BLACK

	def swapColor(self):

		if self.symbol == WHITE:
			self.symbol = BLACK
		else:
			self.symbol = WHITE

class Ant:

	def __init__(self, locate, head = 1):
		self.head 	= head    #0:N 1:E 2:S 3:W
		self.locate = locate

	def turn(self, dira): #1:Right 0:Left
		if dira:
			self.head = (self.head - 1) % 4
		else:
			self.head = (self.head + 1) % 4

	def move(self, w, h):
		if self.head == 0:
			self.locate = (self.locate[0], max(self.locate[1] - 1, 0))
		elif self.head == 1:
			self.locate = (min(self.locate[0] + 1, w), self.locate[1])
		elif self.head == 2:
			self.locate = (self.locate[0], min(self.locate[1] + 1, h))
		else:
			self.locate = (max(self.locate[0] - 1, 0), self.locate[1])

	def getLocation(self):
		return self.locate

	def getLoc(self, w):
		return self.locate[0] + self.locate[1] * w

	def getDir(self):
		if self.head == 0:
			return "North"
		elif self.head == 1:
			return "East"
		elif self.head == 2:
			return "South"
		else:
			return "West"
	def status(self):
		print("Ant: %d, %d | Head: %s" % (self.locate[0], self.locate[1], self.getDir()))
		

class LangtonAnt:

	def __init__(self, width, height):
		self.xMount = width
		self.yMount = height
		self.ant    = Ant((width // 2, height // 2), random.randint(0, 3))
		self.cells  = []
		self.initChess()

	def initChess(self):
		self.cells = [[Cell((i, j), WHITE) for i in range(self.xMount)]
					for j in range(self.yMount)]

	def getX(self):
		return self.xMount

	def getY(self):
		return self.yMount

	def getSymbol(self, locate):
		if (0 > locate[0] > self.xMount - 1 or 0 > locate[1] > self.yMount - 1):
			raise Exception("OOL")
		return self.cells[locate[1]][locate[0]].getSymbol()

	def getCell(self, x, y):
		return self.cells[y][x]

	def getAnt(self):
		return self.ant.getLocation()

	def swapColor(self, locate):
		self.cells[locate[1]][locate[0]].swapColor()

	def step(self):
		loc = self.ant.getLocation()

		if (self.getSymbol(loc) == WHITE):
			self.ant.turn(0)
		else:
			self.ant.turn(1)

		self.swapColor(loc)
		self.ant.move(self.xMount - 1, self.yMount - 1)
		

	def show(self):
		self.ant.status()
		ans = '\n'.join(' '.join(self.cells[j][i].getSymbol() for i in range(self.xMount))
					for j in range(self.yMount))
		ane = self.ant.getLoc(self.xMount)
		ans = ans[:ane * 12] + ANT + ans[ane * 12 + 11:]
		print(ans)


if __name__ == "__main__":
	w = 20
	h = 20
	s = 50
	world = LangtonAnt(w, h)
	world.show()
	while s > 0:
		time.sleep(0.75)
		s -= 1
		os.system('clear')
		world.step()
		world.show()
		
		
	