
import os , pygame

SCREEN_SIZE 		= (801 , 801)	#屏幕尺寸
BLOCK_SIZE			= 4
BLANK_SIZE			= 1
GAME_SPEED 			= 0.02			#游戏运行速率

COLOR_WHITE 		= (75, 170, 200)		
COLOR_BLACK 		= (0, 0, 0)
COLOR_ANT			= (234, 56, 78)
COLOR_BACKGROUND 	= (25, 25, 25)
